package ar.edu.undec.adapter.data.curso.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "curso")
public class CursoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "fecha_cierre_inscripcion")
    private LocalDate fechaCierreInscripcion;

    @Column(name = "nivel")
    private String nivel;

    public CursoEntity(Integer id, String nombre, LocalDate fechaCierreInscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fechaCierreInscripcion = fechaCierreInscripcion;
        this.nivel = nivel;
    }

    public CursoEntity() {

    }
}
