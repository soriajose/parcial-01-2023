package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.ConsultarCursoCRUD;
import curso.input.ConsultarCursoInput;
import curso.modelo.Curso;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ConsultarCursoRepositorioImplementacion implements ConsultarCursoInput {

    ConsultarCursoCRUD consultarCursoCRUD;

    public ConsultarCursoRepositorioImplementacion(ConsultarCursoCRUD consultarCursoCRUD) {
        this.consultarCursoCRUD = consultarCursoCRUD;
    }

    @Override
    public Collection<Curso> consultarCursos() {
        return this.consultarCursoCRUD.findAll();
    }

    @Override
    public Collection<Curso> obtenerCursos() {
        return this.consultarCursoCRUD.findAll();
    }
}
