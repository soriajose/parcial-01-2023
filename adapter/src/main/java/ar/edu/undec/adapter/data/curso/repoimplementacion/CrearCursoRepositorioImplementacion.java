package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.CrearCursoCrud;
import curso.modelo.Curso;
import curso.output.CrearCursoRepositorio;
import org.springframework.stereotype.Service;

@Service
public class CrearCursoRepositorioImplementacion implements CrearCursoRepositorio {

    CrearCursoCrud crearCursoCrud;

    public CrearCursoRepositorioImplementacion(CrearCursoCrud crearCursoCrud) {
        this.crearCursoCrud = crearCursoCrud;
    }

    @Override
    public boolean existePorNombre(String nombre) {
        return this.crearCursoCrud.existsByNombre(nombre);
    }

    @Override
    public Integer guardar(Curso curso) {
        return this.crearCursoCrud.save(curso);
    }
}
