package ar.edu.undec.adapter.service.curso.beanconfig;

import curso.output.CrearCursoRepositorio;
import curso.usecase.CrearCursoUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CrearCursoBinConfig {

    @Bean
    public CrearCursoUseCase crearCursoUseCase(CrearCursoRepositorio crearCursoRepositorio) {
        return new CrearCursoUseCase(crearCursoRepositorio);
    }

}
