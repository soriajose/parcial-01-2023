package ar.edu.undec.adapter.service.curso.controller;

import ar.edu.undec.adapter.service.curso.mapper.Mapper;
import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.input.ConsultarCursoInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cursos")
@CrossOrigin(origins = "*")
public class ConsultarCursosController {

    ConsultarCursoInput consultarCursoInput;

    public ConsultarCursosController(ConsultarCursoInput consultarCursoInput) {
        this.consultarCursoInput = consultarCursoInput;
    }

    @GetMapping
    public ResponseEntity<List<CursoDTO>> consultarCursos() {
        try {
            return ResponseEntity.ok(Mapper.collectionToList(this.consultarCursoInput.consultarCursos()));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


}
