package ar.edu.undec.adapter.service.curso.controller;

import ar.edu.undec.adapter.service.curso.mapper.Mapper;
import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.input.CrearCursoInput;
import curso.modelo.Curso;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cursos")
@CrossOrigin(origins = "*")
public class CrearCursoController {

    CrearCursoInput crearCursoInput;

    public CrearCursoController(CrearCursoInput crearCursoInput) {
        this.crearCursoInput = crearCursoInput;
    }

    @PostMapping
    public ResponseEntity<?> crearCurso(@RequestBody CursoDTO cursoDTO) {
        try {
            return ResponseEntity.created(null).body(this.crearCursoInput.crearCurso(Mapper.dtoToCore(cursoDTO)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(201);
        }
    }

}
