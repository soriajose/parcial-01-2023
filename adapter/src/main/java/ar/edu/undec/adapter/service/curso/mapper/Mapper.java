package ar.edu.undec.adapter.service.curso.mapper;

import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.modelo.Curso;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper {

    public static Curso dtoToCore(CursoDTO cursoDTO) {
        try {
            return new Curso(cursoDTO.getId(), cursoDTO.getNombre(), cursoDTO.getFechaCierreInscripcion(), cursoDTO.getNivel());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CursoDTO coreToDto(Curso curso) {
        try {
            return new CursoDTO(curso.getId(), curso.getNombre(), curso.getFechaCierreInscripcion(), curso.getNivel());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static List<CursoDTO> collectionToList(Collection<Curso> collection) {
        return collection.stream().map(Mapper::coreToDto).collect(Collectors.toList());
    }

}
