package curso.exception;

public class CursoIncorrectoException extends RuntimeException{
    public CursoIncorrectoException(String message) {
        super(message);
    }
}
