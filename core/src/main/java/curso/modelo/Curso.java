package curso.modelo;

import curso.exception.CursoIncompletoException;
import curso.exception.CursoIncorrectoException;

import java.time.LocalDate;

public class Curso {

    private Integer id;
    private String nombre;
    private LocalDate fechaCierreInscripcion;
    private String nivel;

    public Curso(Integer id, String nombre, LocalDate fechaCierreInscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fechaCierreInscripcion = fechaCierreInscripcion;
        this.nivel = nivel;
    }

    public static Curso instancia(Integer id, String nombre, LocalDate fechaCierreInscripcion, String nivel) {
        if (nombre == null || nombre.isBlank()) {
            throw new CursoIncompletoException("El nombre del Curso es obligatorio");
        }
        if (fechaCierreInscripcion == null) {
            throw new CursoIncompletoException("La fecha de cierre del Curso es obligatoria");
        } else {
            if (fechaCierreInscripcion.isBefore(LocalDate.now())) {
                throw new CursoIncorrectoException("La fecha de cierre del Curso no puede ser inferior a la actual");
            }
        }
        if (nivel == null || nivel.isBlank()) {
            throw new CursoIncompletoException("El nivel del Curso es obligatorio");
        }
        return new Curso(id, nombre, fechaCierreInscripcion, nivel);
    }

    public String getNombre() {
        return nombre;
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getFechaCierreInscripcion() {
        return fechaCierreInscripcion;
    }

    public String getNivel() {
        return nivel;
    }
}
