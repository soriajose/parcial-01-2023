package curso.output;

import curso.modelo.Curso;

public interface CrearCursoRepositorio {
    boolean existePorNombre(String nombre);
    Integer guardar(Curso curso);
}
