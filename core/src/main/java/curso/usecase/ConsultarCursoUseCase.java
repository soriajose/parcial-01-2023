package curso.usecase;

import curso.input.ConsultarCursoInput;
import curso.modelo.Curso;
import curso.output.ConsultarCursoRepositorio;

import java.util.Collection;

public class ConsultarCursoUseCase implements ConsultarCursoInput {

    public ConsultarCursoRepositorio consultarCursoRepositorio;
    public ConsultarCursoUseCase(ConsultarCursoRepositorio consultarCursoRepositorio) {
        this.consultarCursoRepositorio = consultarCursoRepositorio;
    }

    @Override
    public Collection<Curso> consultarCursos() {
        return this.consultarCursoRepositorio.obtenerCurso();
    }

    @Override
    public Collection<Curso> obtenerCursos() {
        return this.consultarCursoRepositorio.obtenerCurso();
    }
}
